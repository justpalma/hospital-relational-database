
CREATE DOMAIN NUMEROID
AS INTEGER
CHECK
(
	VALUE >=1
);

CREATE TABLE OSS
(
	CF CHAR(16) PRIMARY KEY,
	Nome VARCHAR(25) NOT NULL,
	Cognome VARCHAR(25) NOT NULL,
	Sesso CHAR(1) NOT NULL,
	Data_nascita DATE NOT NULL,
	Mansione VARCHAR(30) NOT NULL,
	
	CHECK(LENGTH(CF)=16),
	CHECK(Sesso = 'M' or Sesso = 'F' or Sesso = 'm' or Sesso = 'f' )
);

CREATE TABLE Piano
(
	Numero INTEGER NOT NULL,
	Indirizzo VARCHAR(50) NOT NULL,
	PRIMARY KEY(Numero,Indirizzo),
	CHECK(Numero >= 0)
);

CREATE TABLE Reparto
(
	Nome VARCHAR(30) PRIMARY KEY,
	Codice INTEGER NOT NULL,
	Ntelefono CHAR(10),
	Descrizione	VARCHAR(500) NOT NULL,
	Orario_Visite VARCHAR(100),
	Indirizzo_piano VARCHAR(50) NOT NULL,
	Numero_piano INTEGER NOT NULL,
	FOREIGN KEY(Numero_piano,Indirizzo_piano) references Piano(Numero,Indirizzo)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	CONSTRAINT AK_CH UNIQUE(Codice,Indirizzo_piano,Numero_piano)
);

CREATE TABLE Chirurgia
(
	Nome VARCHAR(30) PRIMARY KEY,
	FOREIGN KEY(Nome) references Reparto(Nome)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Chirurgo
(
	Matricola CHAR(6) PRIMARY KEY,
	CF CHAR(16) NOT NULL UNIQUE, --alternative key
	Nome VARCHAR(25) NOT NULL,
	Cognome VARCHAR(25) NOT NULL,
	Sesso CHAR(1) NOT NULL,
	Data_nascita DATE NOT NULL,
	Nome_reparto VARCHAR(30) NOT NULL,
	FOREIGN KEY(Nome_reparto) references Chirurgia(Nome)
		ON UPDATE CASCADE ON DELETE NO ACTION,	
	CHECK(LENGTH(CF)=16),
	CHECK(LENGTH(Matricola)=6),
	CHECK(Sesso = 'M' or Sesso = 'F' or Sesso = 'm' or Sesso = 'f' ),
	CHECK(Matricola like 'CH-%')
);


CREATE TABLE Medico
(
	Matricola CHAR(6) PRIMARY KEY,
	CF CHAR(16) NOT NULL UNIQUE, --alternative key
	Nome VARCHAR(25) NOT NULL,
	Cognome VARCHAR(25) NOT NULL,
	Sesso CHAR(1) NOT NULL,
	Data_nascita DATE NOT NULL,
	CHECK(LENGTH(CF)=16),
	CHECK(LENGTH(Matricola)=6),
	CHECK(Sesso = 'M' or Sesso = 'F' or Sesso = 'm' or Sesso = 'f' ),
	CHECK(Matricola like 'MD-%')
);

CREATE TABLE Infermiere
(
	CF CHAR(16) PRIMARY KEY,
	Nome VARCHAR(25) NOT NULL,
	Cognome VARCHAR(25) NOT NULL,
	Sesso CHAR(1) NOT NULL,
	Data_nascita DATE NOT NULL,
	CHECK(LENGTH(CF)=16),
	CHECK(Sesso = 'M' or Sesso = 'F' or Sesso = 'm' or Sesso = 'f' )
);

CREATE TABLE Reparto_Ospedaliero
(
	Nome VARCHAR(30) PRIMARY KEY,
	FOREIGN KEY(Nome) references Reparto(Nome)
		ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE Primario
(
	CF CHAR(16) PRIMARY KEY, 
	Nome VARCHAR(25) NOT NULL,
	Cognome VARCHAR(25) NOT NULL,
	Sesso CHAR(1) NOT NULL,
	Data_nascita DATE NOT NULL,
	Nome_reparto VARCHAR(30) UNIQUE NOT NULL, --CHIAVE ALTERNATIVA
	FOREIGN KEY(Nome_reparto) references Reparto(Nome)
		ON UPDATE CASCADE ON DELETE CASCADE,	
	CHECK(LENGTH(CF)=16),
	CHECK(Sesso = 'M' or Sesso = 'F' or Sesso = 'm' or Sesso = 'f' )
);

CREATE TABLE Guardia_Giurata
(
	Codice CHAR(6) PRIMARY KEY,
	Ditta VARCHAR(30) NOT NULL,
	Referente VARCHAR(25)NOT NULL,
	CF CHAR(16) NOT NULL UNIQUE, --alternative key
	Nome VARCHAR(25) NOT NULL,
	Cognome VARCHAR(25) NOT NULL,
	Sesso CHAR(1) NOT NULL,
	Data_nascita DATE NOT NULL,
	CHECK(LENGTH(CF)=16),
	CHECK(LENGTH(Codice)=6),
	CHECK(Sesso = 'M' or Sesso = 'F' or Sesso = 'm' or Sesso = 'f' )
);

CREATE TABLE Addetto_Pulizie
(
	Codice CHAR(6) PRIMARY KEY,
	Ditta VARCHAR(30) NOT NULL,
	Referente VARCHAR(25)NOT NULL,
	CF CHAR(16) NOT NULL UNIQUE,	--alternative key
	Nome VARCHAR(25) NOT NULL,
	Cognome VARCHAR(25) NOT NULL,
	Sesso CHAR(1) NOT NULL,
	Data_nascita DATE NOT NULL,
	CHECK(LENGTH(CF)=16),
	CHECK(LENGTH(Codice)=6),
	CHECK(Sesso = 'M' or Sesso = 'F' or Sesso = 'm' or Sesso = 'f' )
);

CREATE TABLE Paziente
(
	CF CHAR(16) PRIMARY KEY,
	Nome VARCHAR(25) NOT NULL,
	Cognome VARCHAR(25) NOT NULL,
	Sesso CHAR(1) NOT NULL,
	Data_nascita DATE NOT NULL,
	Provincia char(2) NOT NULL,
	CHECK(LENGTH(CF)=16),
	CHECK(Sesso = 'M' or Sesso = 'F' or Sesso = 'm' or Sesso = 'f' )
);

CREATE TABLE Paziente_Day_Hospital
(
	CF CHAR(16) PRIMARY KEY,
	FOREIGN KEY(CF) references Paziente(CF)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Camera
(
	Numero NUMEROID NOT NULL,
	Nome_reparto VARCHAR(30) NOT NULL,
	PRIMARY KEY(Numero,Nome_reparto),
	FOREIGN KEY(Nome_reparto) references Reparto_Ospedaliero(Nome) 
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Letto
(
	Numero NUMEROID NOT NULL,
	Numero_camera NUMEROID NOT NULL,
	Nome_reparto VARCHAR(30) NOT NULL,
	Occupato BOOLEAN NOT NULL,
	PRIMARY KEY(Numero,Numero_camera,Nome_reparto),
	FOREIGN KEY(Numero_camera,Nome_reparto) references Camera(Numero,Nome_reparto)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Paziente_Ricoverato
(
	CF CHAR(16) PRIMARY KEY,
	N_Telefono_Familiare CHAR(10) NOT NULL,
	Matricola_medico CHAR(6) NOT NULL,
	Nome_reparto VARCHAR(30) NOT NULL,
	Numero_letto NUMEROID NOT NULL,
	Numero_camera NUMEROID NOT NULL,
	CONSTRAINT AK_Paziente_Ricoverato UNIQUE(Nome_reparto, Numero_Letto, Numero_Camera),
	FOREIGN KEY(CF) references Paziente(CF)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(Matricola_medico) references Medico(Matricola)
		ON UPDATE CASCADE ON DELETE NO ACTION,
	FOREIGN KEY(Numero_letto, Numero_camera, Nome_reparto) references Letto
		ON UPDATE CASCADE ON DELETE NO ACTION
);

CREATE TABLE Pronto_Soccorso
(
	Nome VARCHAR(30) PRIMARY KEY,
	N_telefono_Emeregenze CHAR(10) NOT NULL,
	FOREIGN KEY(Nome) references Reparto(Nome)
		ON UPDATE CASCADE ON DELETE CASCADE
	
);

CREATE TABLE PazientePS
(
	CF CHAR(16) PRIMARY KEY,
	CodicePS CHAR(1) NOT NULL,
	Nome_reparto VARCHAR(30) NOT NULL,
	FOREIGN KEY(CF) references Paziente(CF)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(Nome_reparto) references Pronto_Soccorso(Nome)
		ON UPDATE CASCADE ON DELETE CASCADE,
	CHECK(CodicePS = 'R' or CodicePS = 'V' or CodicePS = 'G' or CodicePS = 'B')
);


CREATE TABLE Turno
(
	Tdata DATE NOT NULL,
	Codice_Guardia CHAR(6) NOT NULL,
	Numero_piano INTEGER NOT NULL,
	Indirizzo_piano VARCHAR(50) NOT NULL,
	PRIMARY KEY(Tdata,Codice_Guardia),
	FOREIGN KEY(Numero_piano,Indirizzo_piano) references Piano(Numero,Indirizzo)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(Codice_Guardia) references Guardia_Giurata(Codice)
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT AK_Turno UNIQUE(Tdata,Numero_piano,Indirizzo_piano) --alternative key
);

CREATE TABLE Sala_Operatoria
(
	Numero NUMEROID PRIMARY KEY,
	Nome_reparto VARCHAR(30) NOT NULL,
	FOREIGN KEY(Nome_reparto) references Chirurgia(Nome)
);

CREATE TABLE Ambulatorio
(
	Codice NUMEROID PRIMARY KEY,
	Nome varchar(25) NOT NULL,
	Nome_reparto varchar(30) NOT NULL,
	FOREIGN KEY(Nome_reparto) references Reparto_Ospedaliero(Nome)
		ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE Visita
(
	Vdata DATE NOT NULL,
	Matricola_medico CHAR(6) NOT NULL,
	CFpaziente CHAR(16) NOT NULL,
	Descrizione VARCHAR(500) NOT NULL,
	Codice_ambulatorio NUMEROID NOT NULL,
	PRIMARY KEY(Vdata,Matricola_medico,CFpaziente),
	FOREIGN KEY(Matricola_medico) REFERENCES Medico(Matricola)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(CFpaziente) REFERENCES Paziente(CF)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(Codice_ambulatorio) REFERENCES Ambulatorio(Codice)
		ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE Cartella_Clinica
(
	CFpaziente CHAR(16) UNIQUE NOT NULL,	--alternative key
	Numero NUMEROID PRIMARY KEY,
	Inizio_Ricovero DATE NOT NULL,
	Fine_Ricovero DATE,
	Motivo_Ricovero VARCHAR(500),
	FOREIGN KEY(CFpaziente) references Paziente_Ricoverato(CF)
		ON UPDATE CASCADE ON DELETE CASCADE,
	CHECK
	(
		Inizio_Ricovero < Fine_Ricovero
	)
);

CREATE TABLE Operazione_Chirurgica
(
	OCdata DATE NOT NULL,
	CHMatricola CHAR(16) NOT NULL,
	CFpaziente CHAR(16) NOT NULL,
	Tipo VARCHAR(500) NOT NULL,
	Esito VARCHAR(500) NOT NULL,
	Numero_sala NUMEROID NOT NULL,
	PRIMARY KEY(OCdata,CHmatricola),
	FOREIGN KEY(CHMatricola) references Chirurgo(Matricola)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(CFpaziente) references Paziente_Ricoverato(CF)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(Numero_sala) references Sala_Operatoria(Numero)
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT AK_Operazione UNIQUE(CFpaziente,OCdata)
);

CREATE TABLE Consulenza
(
	CFpaziente CHAR(16) NOT NULL,
	CFOSS CHAR(16) NOT NULL,
	Consulenza_data DATE NOT NULL,
	Commento_Medico VARCHAR(500) NOT NULL,
	FOREIGN KEY(CFpaziente) references Paziente(CF)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(CFOSS) references OSS(CF)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Assegnazione_Medico
(
	Matricola_medico CHAR(6) NOT NULL,
	Nome_reparto VARCHAR(30) NOT NULL,
	PRIMARY KEY(Matricola_medico,Nome_reparto),
	FOREIGN KEY(Matricola_medico) references Medico(Matricola)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(Nome_reparto) references Reparto(Nome)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Assegnazione_Infermiere
(
	CFinfermiere CHAR(16) NOT NULL,
	Nome_reparto VARCHAR(30) NOT NULL,
	PRIMARY KEY(CFinfermiere,Nome_reparto),
	FOREIGN KEY(CFinfermiere) references Infermiere(CF)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(Nome_reparto) references Reparto(Nome)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Assegnazione_Pulizie
(
	Codice_addetto CHAR(6) NOT NULL,
	Nome_reparto VARCHAR(30) NOT NULL,
	PRIMARY KEY(Codice_addetto,Nome_reparto),
	FOREIGN KEY(Codice_addetto) references Addetto_Pulizie(Codice)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(Nome_reparto) references Reparto(Nome)
		ON UPDATE CASCADE ON DELETE CASCADE
);


--trigger che verifica che un medico abbia al massimo 10 pazieti
CREATE OR REPLACE FUNCTION max_paz_med() RETURNS trigger AS $max_paz_med$
BEGIN
IF (select count(*) from Paziente_ricoverato WHERE NEW.Matricola_medico=Paziente_ricoverato.Matricola_medico) >= 10
THEN RAISE EXCEPTION 'Questo medico ha già 10 pazienti a suo seguito';
END IF;
RETURN NEW;
END;
$max_paz_med$ LANGUAGE plpgsql;

CREATE TRIGGER Medico_Pazienti_Max
BEFORE INSERT OR UPDATE
ON Paziente_ricoverato for each row
EXECUTE PROCEDURE max_paz_med();

--trigger che imposta il letto occupato dopo un inserimento
CREATE OR REPLACE FUNCTION aggiorna_letto() returns trigger AS $aggiorna_letto$
BEGIN
UPDATE Letto SET Occupato=true 
where NEW.Numero_letto=Letto.Numero and NEW.Numero_camera=Letto.Numero_camera and NEW.Nome_reparto=Letto.Nome_reparto;
RETURN NEW;
END;
$aggiorna_letto$ LANGUAGE plpgsql;

CREATE TRIGGER Inserimento_Paziente_Letto
AFTER INSERT ON Paziente_ricoverato for each row
EXECUTE PROCEDURE aggiorna_letto();

--trigger che verifica che un medico sia associato ad un max di 2 reparti
CREATE OR REPLACE FUNCTION max_rep_med() RETURNS trigger AS $max_rep_med$
BEGIN
IF (select count(*) from Assegnazione_Medico WHERE NEW.Matricola_medico=Assegnazione_Medico.Matricola_medico) >= 2
THEN RAISE EXCEPTION 'Questo medico è già assegnato a 2 reparti';
END IF;
RETURN NEW;
END;
$max_rep_med$ LANGUAGE plpgsql;

CREATE TRIGGER Medico_Reparto_Max
BEFORE INSERT OR UPDATE
ON Assegnazione_Medico for each row
EXECUTE PROCEDURE max_rep_med();


--trigger che limita le associazioni addetti pulizie e reparti fino ad un massimo di 3
CREATE OR REPLACE FUNCTION max_pul_rep() RETURNS trigger AS $max_pul_rep$
BEGIN
IF (select count(*) from Assegnazione_Pulizie WHERE NEW.Codice_addetto=Assegnazione_Pulizie.Codice_addetto) >= 3
THEN RAISE EXCEPTION 'Questo addetto alle pulizie è già assegnato a 3 reparti';
END IF;
RETURN NEW;
END;
$max_pul_rep$ LANGUAGE plpgsql;

CREATE TRIGGER Pulizie_Reparto_Max
BEFORE INSERT OR UPDATE
ON Assegnazione_Pulizie for each row
EXECUTE PROCEDURE max_pul_rep();

--trigger che scatta se la data di fine ricovero in una cartella clinica precede quella di inizio ricovero
CREATE OR REPLACE FUNCTION check_data() RETURNS trigger AS $check_data$
BEGIN
IF (select CC.inizio_ricovero from Cartella_Clinica CC WHERE NEW.numero=CC.numero) > NEW.fine_ricovero
THEN RAISE EXCEPTION 'La fine di un ricovero non può essere precedere l''inizio di esso, controllare la data di fine ricovero';
END IF;
RETURN NEW;
END;
$check_data$ LANGUAGE plpgsql;

CREATE TRIGGER Controllo_Data_CC
BEFORE UPDATE
ON Cartella_Clinica for each row
EXECUTE PROCEDURE check_data();

--procedura che assegna un cartella clinica ad un paziente ricoverato immesso nel sistema
CREATE OR REPLACE FUNCTION Aggiungi_CC() RETURNS trigger AS $Aggiungi_CC$
BEGIN
IF (select count(*) from Cartella_Clinica) = 0 THEN
	INSERT INTO Cartella_Clinica Values(NEW.CF,1,CURRENT_DATE,null,null);
ELSE
	INSERT INTO Cartella_Clinica Values(NEW.CF,(select max(numero)+1 from cartella_clinica),CURRENT_DATE,null,null);
END IF;
RETURN NEW;
END;
$Aggiungi_CC$ LANGUAGE plpgsql;

CREATE TRIGGER Insert_CC
AFTER INSERT
ON Paziente_Ricoverato for each row
EXECUTE PROCEDURE Aggiungi_CC();


--trigger che limita massimo 4 letti per camera
CREATE OR REPLACE FUNCTION max_letti_camera() RETURNS trigger AS $max_letti_camera$
BEGIN
IF (select count(*) from Letto WHERE NEW.Nome_reparto=Letto.Nome_reparto and NEW.Numero_camera=Letto.Numero_camera) >= 4
THEN RAISE EXCEPTION 'Questa camera possiede già il numero di letti massimo';
END IF;
RETURN NEW;
END;
$max_letti_camera$ LANGUAGE plpgsql;

CREATE TRIGGER Letti_camera_max
BEFORE INSERT
ON Letto for each row
EXECUTE PROCEDURE max_letti_camera();


--trigger che aggiunge 1 letto per camera
CREATE OR REPLACE FUNCTION aggiungi_letto() RETURNS trigger AS $aggiungi_letto$
BEGIN
INSERT INTO Letto Values(1, NEW.Numero,NEW.Nome_reparto,false);
RETURN NEW;
END;
$aggiungi_letto$ LANGUAGE plpgsql;

CREATE TRIGGER Aggiunta_letto_camera
AFTER INSERT 
ON Camera for each row
EXECUTE PROCEDURE aggiungi_letto();


--procedura che crea almeno 3 camere dopo aver inserito un reparto ospedaliero
CREATE OR REPLACE FUNCTION aggiungi_camera() RETURNS trigger AS $aggiungi_camera$
BEGIN
INSERT INTO Camera Values(1, NEW.Nome);
INSERT INTO Camera Values(2, NEW.Nome);
INSERT INTO Camera Values(3, NEW.Nome);
RETURN NEW;
END;
$aggiungi_camera$ LANGUAGE plpgsql;

CREATE TRIGGER Aggiunta_camera_reparto
AFTER INSERT 
ON Reparto_ospedaliero for each row
EXECUTE PROCEDURE aggiungi_camera();


