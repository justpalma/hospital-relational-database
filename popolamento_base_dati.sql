INSERT INTO OSS VALUES ('PLMGRN79P12E136M', 'Guerrino', 'Palumbo', 'M', '1979-09-12','Psicologo');
INSERT INTO OSS VALUES ('SNCMXA87C19H121Z', 'Max', 'Sanchez', 'M', '1987-03-19', 'Diestista');
INSERT INTO OSS VALUES ('DMRLHR86C07G331F', 'Lothar', 'De maria', 'M', '1986-03-09','Fisioterapista');
INSERT INTO OSS VALUES ('CHSLEI50B08L905Z', 'Elio', 'Chessa', 'M', '1950-02-08','Igienista Dentale');
INSERT INTO OSS VALUES ('PGGTLM96C22H478K', 'Tolomeo', 'Poggi', 'M', '1996-03-22','Podologo');
INSERT INTO OSS VALUES ('MGRRRA66C60L904B', 'Aurora', 'Maugeri', 'F', '1966-03-20','Logopedista');
INSERT INTO OSS VALUES ('FRVBGT99P60E381E', 'Brigitta', 'Fioravanti', 'F', '1999-09-20','Ostetricia');
INSERT INTO OSS VALUES ('PTRRNG50T59C993J', 'Rosangela', 'Patrizi', 'F', '1950-12-19','Dietista');
INSERT INTO OSS VALUES ('SMNMGD60R44L107I', 'Magda', 'Simeone', 'F', '1960-10-04','Fisioterapista');
INSERT INTO OSS VALUES ('GDCGVF99A67A273N', 'Genoveffa', 'Giudici', 'F', '1999-01-27','Psicologo');

INSERT INTO Piano VALUES(0,'Corso Vercelli 74');
INSERT INTO Piano VALUES(1,'Corso Vercelli 74');
INSERT INTO Piano VALUES(2,'Corso Vercelli 74');
INSERT INTO Piano VALUES(3,'Corso Vercelli 74');
INSERT INTO Piano VALUES(4,'Corso Vercelli 74');
INSERT INTO Piano VALUES(0,'Via Campi 12');
INSERT INTO Piano VALUES(1,'Via Campi 12');
INSERT INTO Piano VALUES(0,'Via Gramsci');
INSERT INTO Piano VALUES(0,'Via Verdi');
INSERT INTO Piano VALUES(0,'Via Guarini');
--nome codice ntelefono descrizione orario visite indirizzo numero piano
INSERT INTO Reparto Values('Cardiologia A', 01, '0593961172', 'Il Reparto di Cardiologia rappresenta un importante punto di riferimento per la diagnosi e la terapia delle malattie cardiovascolari', '8.00 - 9.00; 12.30 - 13.30; 18.30 - 19.30', 'Corso Vercelli 74', 0);
INSERT INTO Reparto Values('Centro Dialisi', 02, '0593961199 ', 'Al Centro Dialisi vengono sottoposti ad un processo di depurazione del sangue i pazienti affetti da insufficienza renale cronica per i quali la dialisi rappresenta un intervento salvavita.', null, 'Corso Vercelli 74', 0);
INSERT INTO Reparto Values('Endocrinologia', 03, '0593961829', 'L’attività assistenziale è rivolta alla diagnosi e cura di patologie endocrine, metaboliche e andrologiche. Le specifiche competenze, riconosciute a livello nazionale e internazionale, riguardano la patologia tiroidea con particolare riferimento ai tumori della tiroide, la patologia ipofisaria, la sessuologia, l’infertilità maschile, l’obesità e le sue complicanze, i disturbi endocrino-metabolici dell’osso, le patologie surrenaliche e le malattie rare', '6.30 - 9.00; 12.15 - 15.00; 18.15 - 21.00', 'Corso Vercelli 74', 0);
INSERT INTO Reparto Values('Gastroenterologia Digestiva', 04, '0593961220', 'L’obiettivo primario è quello di rispondere alla totalità del fabbisogno della popolazione attraverso l’offerta di prestazioni endoscopiche quali gastroscopie, colonscopie (anche di screening) procedure del tratto biliare e pancreatico, esami eseguiti con la videocapsula, ecoendoscopie erogati con standard medio-alti', null, 'Corso Vercelli 74', 0);
INSERT INTO Reparto Values('Geriatria', 05, '0593961027', 'Questa Struttura si prefigge di curare e assistere gli anziani che presentano malattie acute, la riacutizzazione di malattie croniche o disabilità complesse.', '6.30 - 9.00; 12.00 - 15.00; 18.00 - 20.30', 'Corso Vercelli 74', 1);
INSERT INTO Reparto Values('Cardiologia B', 01, '0593961172', 'Il Reparto di Cardiologia rappresenta un importante punto di riferimento per la diagnosi e la terapia delle malattie cardiovascolari', '8.00 - 9.00; 12.30 - 13.30; 18.30 - 19.30', 'Corso Vercelli 74', 1);
INSERT INTO Reparto Values('Medicina Interna', 06, '0593961122', 'Questa Struttura si occupa di pazienti con problematiche di competenza internistica o malattie dell’apparato digerente', '6.30 - 9.00; 12.30 - 14.00; 18.30 - 21.30', 'Corso Vercelli 74', 1);
INSERT INTO Reparto Values('Medicina Riabilitativa', 07, '0593961933', 'Questa Struttura accoglie i pazienti con disabilità conseguenti a una o più lesioni traumatiche o a esiti evolutivi di malattie neurologiche (stroke ischemico ed emorragico, gravi cerebrolesioni acquisite o altre gravi disabilità) o esiti di lesioni all’apparato muscolo-scheletrico', '8.00 - 9.00; 12.15 - 14.00; 18.15 - 21.00', 'Corso Vercelli 74', 1);
INSERT INTO Reparto Values('Neurochirurgia', 08, '0593961471', 'Neurochirurgia accoglie e cura pazienti con patologie del sistema nervoso centrale e periferico su cui si può intervenire chirurgicamente', '6.30 - 9.00; 12.00 - 14.00; 18.00 - 21.00', 'Corso Vercelli 74', 2);
INSERT INTO Reparto Values('Chirurgia della Mano', 18, '0594222111', 'Chirurgia della Mano e Microchirurgia rappresenta il Centro di Eccellenza e di Riferimento regionale per la chirurgia della mano e la microchirurgia nell’ambito del sistema Hub and Spoke per la diagnosi e trattamento di tutta la patologia della mano e del polso, sia essa traumatica, degenerativa, congenita, neurologica', '6.30-8.00; 12.00-14.00; 16.30-20.00', 'Corso Vercelli 74', 3);
INSERT INTO Reparto Values('Chirurgia Pediatrica', 19, '0594225504', 'Chirurgia Pediatrica affronta tutto l’ampio spettro della patologia chirurgica viscerale e urologica dell’età neonatale e pediatrica, congenita o acquisita, compresa la chirurgia oncologica, d’elezione od in urgenza', '6.30-9.00; 12.30-15.30; 18.30-21.00', 'Corso Vercelli 74', 3);
INSERT INTO Reparto Values('Chirurgia Toracica', 09, '0594222412', 'La Struttura Complessa di Chirurgia Toracica tratta tutte le patologie del distretto toracico di interesse chirurgico', '6.30-8.30; 12.00-14.30; 18.30-21.00', 'Corso Vercelli 74', 3);
INSERT INTO Reparto Values('Pronto Soccorso - Via Gramsci',99,0597895481,'Pronto Soccorso situato in Via Gramsci',null,'Via Gramsci',0);
INSERT INTO Reparto Values('Pronto Soccorso - Via Verdi',99,0597174893,'Pronto Soccorso situato in Via Verdi',null,'Via Verdi',0);
INSERT INTO Reparto Values('Pronto Soccorso - Via Guarini',99,0597568129,'Pronto Soccorso situato in Via Guarini',null,'Via Guarini',0);

INSERT INTO Chirurgia Values('Chirurgia della Mano');
INSERT INTO Chirurgia Values('Chirurgia Pediatrica');
INSERT INTO Chirurgia Values('Chirurgia Toracica');

--matricola cf nome cognome sesso data nascita nome reparto

INSERT INTO Chirurgo Values('CH-215','BTATLL53L01F943H','Tullio','Abate','M','1953-07-01','Chirurgia della Mano');
INSERT INTO Chirurgo Values('CH-879','CQVRMG53C25B631X','Remigio','Acquaviva','M','1953-03-25','Chirurgia della Mano');
INSERT INTO Chirurgo Values('CH-027','DNAPTN98D17G735A','Platone','Dani','M','1998-04-17','Chirurgia della Mano');
INSERT INTO Chirurgo Values('CH-669','CLFDRO95D51I544W','Dora','Califano','F','1995-04-11','Chirurgia Toracica');
INSERT INTO Chirurgo Values('CH-128','CRMPNG62P46A967I','Pierangela','Carmen','F','1962-09-06','Chirurgia Toracica');
INSERT INTO Chirurgo Values('CH-748','NZZMGL94P56E500Q','Mariagiulia','Nuzzo','F','1994-09-16','Chirurgia Pediatrica');
INSERT INTO Chirurgo Values('CH-801','LCNLLY84A52E367D','Loreley','Luciano','F','1984-01-12','Chirurgia Pediatrica');
INSERT INTO Chirurgo Values('CH-901','VLLLSS94P56C510B','Alessia','Villa','F','1994-09-16','Chirurgia Pediatrica');

INSERT INTO Medico Values('MD-159','BSCKRL66D17C840U','Karol','Boschi','M','1966-04-17');
INSERT INTO Medico Values('MD-542','QTTLEO84R04C259Q','Leo','Quattrocchi','M','1984-10-04');
INSERT INTO Medico Values('MD-578','MCNFLV99H13A738I','Fulvio','Miconi','M','1999-06-13');
INSERT INTO Medico Values('MD-129','STTNCS80B24E901G','Narciso','Sette','M','1980-02-24');
INSERT INTO Medico Values('MD-070','LFNFBR73B20E132I','Filiberto','Alfano','M','1973-02-20');
INSERT INTO Medico Values('MD-712','CNTDZN54C54H421V','Domiziana','Conti','F','1954-03-14');
INSERT INTO Medico Values('MD-378','FRNNSC61H44H361N','Anusca','Franchini','F','1961-06-04');
INSERT INTO Medico Values('MD-548','LZZRRA84D54F500K','Aurora','Lazzari','F','1984-04-14');
INSERT INTO Medico Values('MD-687','LLLLCU70M56I630H','Lucia','Lilla','F','1970-08-16');
INSERT INTO Medico Values('MD-375','GRFJTN96C48E991O','Justine','Garofalo','F','1996-03-08');
INSERT INTO Medico Values('MD-806','FRNMVT49L42C780U','Maria Vittoria','Franchini','F','1949-07-02');

INSERT INTO Infermiere Values('FRRNKT57D14F920I','Nikita','Ferrero','M','1957-04-14');
INSERT INTO Infermiere Values('MBRFST60M30C240I','Fausto','Ambrosini','M','1960-08-30');
INSERT INTO Infermiere Values('LBRGNR91D18B227A','Gennaro','Liberti','M','1991-04-18');
INSERT INTO Infermiere Values('LNERHL62T42A897A','Rachele','Lena','F','1962-12-02');

INSERT INTO Reparto_Ospedaliero Values('Cardiologia A');
INSERT INTO Reparto_Ospedaliero Values('Centro Dialisi');
INSERT INTO Reparto_Ospedaliero Values('Endocrinologia');
INSERT INTO Reparto_Ospedaliero Values('Gastroenterologia Digestiva');
INSERT INTO Reparto_Ospedaliero Values('Geriatria');
INSERT INTO Reparto_Ospedaliero Values('Cardiologia B');
INSERT INTO Reparto_Ospedaliero Values('Medicina Interna');
INSERT INTO Reparto_Ospedaliero Values('Medicina Riabilitativa');
INSERT INTO Reparto_Ospedaliero Values('Neurochirurgia');


-- n_letto, n_camera, reparto, occupato

INSERT INTO Letto Values(2,1,'Cardiologia A',false);
INSERT INTO Letto Values(3,1,'Cardiologia A',false);
INSERT INTO Letto Values(4,1,'Cardiologia A',false);
INSERT INTO Letto Values(2,2,'Cardiologia A',false);
INSERT INTO Letto Values(2,3,'Cardiologia A',false);
INSERT INTO Letto Values(3,3,'Cardiologia A',false);
INSERT INTO Letto Values(4,3,'Cardiologia A',false);
INSERT INTO Letto Values(2,1,'Cardiologia B',false);
INSERT INTO Letto Values(2,1,'Centro Dialisi',false);
INSERT INTO Letto Values(2,1,'Neurochirurgia',false);



INSERT INTO Primario Values('PRIBSL93H01G816P','Basile','Piri','M','1997-06-01','Cardiologia A');
INSERT INTO Primario Values('MTTMHL94D24A870F','Michelangelo','Mattei','M','1994-04-24','Centro Dialisi');
INSERT INTO Primario Values('QNTTRZ95P23G747L','Terzo','Quinto','M','1995-09-23','Endocrinologia');
INSERT INTO Primario Values('DMRDFN51A04A448I','Delfino','De marco','M','1951-01-04','Gastroenterologia Digestiva');
INSERT INTO Primario Values('DDMLST72E16H166M','Alceste','Di domenico','M','1972-05-16','Geriatria');
INSERT INTO Primario Values('FNTPCS88C16M160P','Piercesare','Finotti','M','1988-03-16','Cardiologia B');
INSERT INTO Primario Values('CZZFDN81R01I115Y','Ferdinando','Cozzolino','M','1981-10-01','Medicina Interna');
INSERT INTO Primario Values('CSTLLE70R16B667G','Lelio','Castaldi','M','1970-10-16','Medicina Riabilitativa');
INSERT INTO Primario Values('LNEDRN99R22D548E','Daran','Daran','M','1999-10-22','Neurochirurgia');
INSERT INTO Primario Values('PZZNRA60L46E135A','Nara','Pizzi','F','196-07-06','Chirurgia della Mano');
INSERT INTO Primario Values('LRNLCN93H41E045H','Lucina','Laurenti','F','1993-06-01','Chirurgia Toracica');
INSERT INTO Primario Values('LVEMFL74H64C172H','Maria Flavia','Levi','F','1974-06-24','Chirurgia Pediatrica');


INSERT INTO Guardia_Giurata Values('259741','Sicurezza SPA','L.Sacco','PRNRSM59M01D539F','Erasmo','Perna','M','1959-08-01');
INSERT INTO Guardia_Giurata Values('846922','Ladri via di qua SCE','A.Corradi','SPRLST74L22A661A','Alceste','Spera','M','1974-07-22');
INSERT INTO Guardia_Giurata Values('487166','Real Security SRLS','C.Toschi','MZZRNT66R04L107G','Renato','Mazzocchi','M','1966-10-04');
INSERT INTO Guardia_Giurata Values('781694','Sono Armato SRL','P.Gervasio','FRRRTS76M25A906X','Artes','Ferrando','M','1976-08-25');

INSERT INTO Addetto_Pulizie Values('789653','Pulisco Bene SPA','R.Toto','CRZDME71D54F370Z','Demi','Corazza','F', '1971-04-14');
INSERT INTO Addetto_Pulizie Values('481981','Clean SRL','O.Lasso','CRDCRS88P47E761F','Clarissa','Cardinali','F', '1988-09-07');


INSERT INTO Paziente Values('DLVCST97R24I261X','Cristian','Del vecchio','M','1997-10-24','MO');
INSERT INTO Paziente Values('DNEGCL85T19G434I','Gianclaudio','Deiana','M','1985-12-19','BO');
INSERT INTO Paziente Values('RSURTM57R15E235W','Artemio','Urso','M','1957-10-15','PA');
INSERT INTO Paziente Values('TRRCRS63B25D944H','Christopher','Terranova','M','1963-02-25','MO');
INSERT INTO Paziente Values('DGNMRI75B19F688W','Imer','Di gennaro','M','1975-02-19','BO');
INSERT INTO Paziente Values('SCTTMS56L20F475N','Thomas','Scotto','M','1956-07-20','RO');
INSERT INTO Paziente Values('PSQVST66P05D539A','Evaristo','Pasquali','M','1966-09-05','BZ');
INSERT INTO Paziente Values('LNRMFR82H06L589R','Manfredi','Leonardi','M','1982-06-06','LI');
INSERT INTO Paziente Values('SCIRMN79A30E089Q','Ramon','Sica','M','1979-01-30','TO');
INSERT INTO Paziente Values('RCCMRC83R15H001L','Marc','Ricco','M','1983-10-15','MO');
INSERT INTO Paziente Values('FRIRLD76H11H768H','Eronaldo','Fiore','M','1976-06-11','MO');
INSERT INTO Paziente Values('GNNSDR66T62D459S','Sandra','Gennari','F','1966-12-22','BO');
INSERT INTO Paziente Values('CLLTRA62P55D790L','Tara','Cella','F','1962-09-15','PA');
INSERT INTO Paziente Values('DFLSSA73P65B506N','Assia','De filippo','F','1973-09-25','MI');
INSERT INTO Paziente Values('NGRVNI71E50H042D','Ivania','Negri','F','1971-05-10','MI');
INSERT INTO Paziente Values('GRFNMR99P44B645Q','Annamaria','Garofalo','F','1998-09-04','SA');
INSERT INTO Paziente Values('MNFVLE84M68C702H','Velia','Manfredi','F','1984-08-28','NA');
INSERT INTO Paziente Values('PRRBNG60B25G454K','Berengario','Perrotta','M','1960-02-25','IM');
INSERT INTO Paziente Values('DRCGST49M20A711T','Egisto','Darco','M','1949-08-20','PV');
INSERT INTO Paziente Values('PRRBRS76P05B902R','Boris','Priori','M','1976-09-05','AL');
INSERT INTO Paziente Values('MRBRRT82H04C708I','Roberto','Mirabella','M','1982-06-04','PA');
INSERT INTO Paziente Values('BCCGGR56L18L566D','Gregorio','Bacci','M','1956-07-18','TO');
INSERT INTO Paziente Values('NNNDNT59H02D773R','Diamante','Annunziata','M','1959-06-02','RM');
INSERT INTO Paziente Values('PPRPFR58R25F182D','Pierfrancesco','Paperino','M','1958-10-25','TO');
INSERT INTO Paziente Values('STLZDA74M02I986H','Azad','Stellina','M','1974-08-02','BG');
INSERT INTO Paziente Values('DMTJZL67P23F433E','Jaziel','D mato','M','1967-09-23','CH');
INSERT INTO Paziente Values('SFRNTS90R22A120F','Anastasio','Sforza','M','1990-10-22','PE');
INSERT INTO Paziente Values('LFRCLL72A25L613Z','Camillo','Alfieri','M','1972-01-25','CA');
INSERT INTO Paziente Values('TRRMRK54P50A268V','Marika','Terranova','F','1954-09-10','NA');
INSERT INTO Paziente Values('LZZRNI66B64D329K','Irene','Lazzaro','F','1966-02-24','CO');
INSERT INTO Paziente Values('RMNDTT60A58B631F','Diletta','Romanelli','F','1960-01-18','RI');
INSERT INTO Paziente Values('SBTMND72M42B267H','Amanda','Sabato','F','1972-08-02','BN');
INSERT INTO Paziente Values('FBRNKA81A49C795U','Naike','Fabris','F','1981-01-09','CS');
INSERT INTO Paziente Values('PLMMLA74A62F731C','Maila','Palmas','F','1974-01-22','SA');
INSERT INTO Paziente Values('PLVLSS87L46C895B','Alessia','Pulvirenti','F','1987-07-06','MI');


INSERT INTO Paziente_Day_Hospital Values('DLVCST97R24I261X');
INSERT INTO Paziente_Day_Hospital Values('DNEGCL85T19G434I');
INSERT INTO Paziente_Day_Hospital Values('RSURTM57R15E235W');
INSERT INTO Paziente_Day_Hospital Values('TRRCRS63B25D944H');
INSERT INTO Paziente_Day_Hospital Values('PLVLSS87L46C895B');
--cf, telefono, medico, reparto, letto, camera
INSERT INTO Paziente_Ricoverato Values('DGNMRI75B19F688W','330939206','MD-159','Cardiologia A',1,1);
INSERT INTO Paziente_Ricoverato Values('SCTTMS56L20F475N','348682589','MD-159','Cardiologia A',1,2);
INSERT INTO Paziente_Ricoverato Values('PSQVST66P05D539A','320224443','MD-159','Cardiologia A',2,1);
INSERT INTO Paziente_Ricoverato Values('LNRMFR82H06L589R','324521483','MD-159','Cardiologia A',3,1);
INSERT INTO Paziente_Ricoverato Values('SCIRMN79A30E089Q','342279409','MD-159','Cardiologia A',4,1);
INSERT INTO Paziente_Ricoverato Values('RCCMRC83R15H001L','320775506','MD-159','Cardiologia A',2,2);
INSERT INTO Paziente_Ricoverato Values('FRIRLD76H11H768H','348753934','MD-159','Cardiologia A',1,3);
INSERT INTO Paziente_Ricoverato Values('GNNSDR66T62D459S','340281579','MD-159','Cardiologia A',2,3);
INSERT INTO Paziente_Ricoverato Values('CLLTRA62P55D790L','327325632','MD-159','Cardiologia A',3,3);
INSERT INTO Paziente_Ricoverato Values('DFLSSA73P65B506N','368823011','MD-159','Cardiologia A',4,3);
INSERT INTO Paziente_Ricoverato Values('NGRVNI71E50H042D','368823011','MD-578','Cardiologia B',1,1);
INSERT INTO Paziente_Ricoverato Values('GRFNMR99P44B645Q','368823011','MD-578','Cardiologia B',1,2);
INSERT INTO Paziente_Ricoverato Values('MNFVLE84M68C702H','368823011','MD-070','Endocrinologia',1,1);
INSERT INTO Paziente_Ricoverato Values('PRRBNG60B25G454K','368823011','MD-070','Gastroenterologia Digestiva',1,1);
INSERT INTO Paziente_Ricoverato Values('DRCGST49M20A711T','330939206','MD-070','Gastroenterologia Digestiva',1,2);
INSERT INTO Paziente_Ricoverato Values('PRRBRS76P05B902R','330939206','MD-687','Medicina Interna',1,1);
INSERT INTO Paziente_Ricoverato Values('MRBRRT82H04C708I','330939206','MD-687','Geriatria',1,1);
INSERT INTO Paziente_Ricoverato Values('BCCGGR56L18L566D','330939206','MD-806','Geriatria',1,2);
INSERT INTO Paziente_Ricoverato Values('NNNDNT59H02D773R','330939206','MD-542','Geriatria',1,3);
INSERT INTO Paziente_Ricoverato Values('PPRPFR58R25F182D','330939206','MD-542','Neurochirurgia',2,1);

--nome, numero telefono
INSERT INTO Pronto_Soccorso Values('Pronto Soccorso - Via Gramsci',0594222111);
INSERT INTO Pronto_Soccorso Values('Pronto Soccorso - Via Verdi',0598942187);
INSERT INTO Pronto_Soccorso Values('Pronto Soccorso - Via Guarini',0592547894);

--cf,codice ps, Nome_reparto
INSERT INTO PazientePS Values('PLMMLA74A62F731C','R','Pronto Soccorso - Via Gramsci');
INSERT INTO PazientePS Values('FBRNKA81A49C795U','R','Pronto Soccorso - Via Gramsci');
INSERT INTO PazientePS Values('SBTMND72M42B267H','B','Pronto Soccorso - Via Gramsci');
INSERT INTO PazientePS Values('RMNDTT60A58B631F','B','Pronto Soccorso - Via Gramsci');
INSERT INTO PazientePS Values('LZZRNI66B64D329K','B','Pronto Soccorso - Via Verdi');
INSERT INTO PazientePS Values('TRRMRK54P50A268V','V','Pronto Soccorso - Via Verdi');
INSERT INTO PazientePS Values('LFRCLL72A25L613Z','V','Pronto Soccorso - Via Guarini');
INSERT INTO PazientePS Values('SFRNTS90R22A120F','G','Pronto Soccorso - Via Guarini');
INSERT INTO PazientePS Values('DMTJZL67P23F433E','G','Pronto Soccorso - Via Guarini');
INSERT INTO PazientePS Values('STLZDA74M02I986H','G','Pronto Soccorso - Via Guarini');
-- data, codice_guardia, numero_piano, indirizzo piano
INSERT INTO Turno Values('2018-05-22','259741','0','Via Campi 12');
INSERT INTO Turno Values('2018-01-12','259741','0','Via Campi 12');
INSERT INTO Turno Values('2019-04-24','846922','1','Corso Vercelli 74');
INSERT INTO Turno Values('2019-05-09','487166','4','Corso Vercelli 74');
INSERT INTO Turno Values('2018-09-08','846922','0','Via Guarini');
INSERT INTO Turno Values('2018-12-24','781694','0','Via Guarini');

--numero, nome reparto
INSERT INTO Sala_Operatoria Values(1,'Chirurgia della Mano');
INSERT INTO Sala_Operatoria Values(2,'Chirurgia della Mano');
INSERT INTO Sala_Operatoria Values(3,'Chirurgia Pediatrica');
INSERT INTO Sala_Operatoria Values(4,'Chirurgia Toracica');
INSERT INTO Sala_Operatoria Values(5,'Chirurgia Toracica');

--Codice,nome,reparto
INSERT INTO Ambulatorio Values(01,'Ambulatorio Geriatria 1','Geriatria');
INSERT INTO Ambulatorio Values(02,'Ambulatorio NeuroCH. 1','Neurochirurgia');
INSERT INTO Ambulatorio Values(03,'Ambulatorio NeuroCH. 2','Neurochirurgia');
INSERT INTO Ambulatorio Values(04,'Ambulatorio M.interna 1','Medicina Interna');
INSERT INTO Ambulatorio Values(05,'Ambulatorio M.interna 2','Medicina Interna');
INSERT INTO Ambulatorio Values(06,'Ambulatorio Cardiologia 1','Cardiologia A');


--data,matricola medico, cfpaziente,descrizione,codiceambulatorio
INSERT INTO Visita Values('2018-06-12','MD-159','DLVCST97R24I261X','Visita oculistica',1);
INSERT INTO Visita Values('2018-06-12','MD-159','STLZDA74M02I986H','Visita dermatologica',1);
INSERT INTO Visita Values('2018-04-03','MD-542','FBRNKA81A49C795U','Visita pediatrica',1);
INSERT INTO Visita Values('2018-08-12','MD-806','LFRCLL72A25L613Z','Visita proctologica',2);
INSERT INTO Visita Values('2018-12-12','MD-712','RMNDTT60A58B631F','Visita reumatologica',2);
INSERT INTO Visita Values('2018-12-12','MD-712','DRCGST49M20A711T','Visita urologica pediatrica',2);
INSERT INTO Visita Values('2018-12-12','MD-712','PLVLSS87L46C895B','Visita vulnologica',2);
INSERT INTO Visita Values('2018-12-12','MD-712','LZZRNI66B64D329K','Visita ematologica',3);
INSERT INTO Visita Values('2018-08-15','MD-129','DMTJZL67P23F433E','Visita epatologica',4);
INSERT INTO Visita Values('2018-07-19','MD-129','FRIRLD76H11H768H','Visita immunologica',5);
INSERT INTO Visita Values('2018-02-08','MD-578','NGRVNI71E50H042D','Visita nefrologica pediatrica',6);
INSERT INTO Visita Values('2018-01-26','MD-548','RSURTM57R15E235W','Visita neurologica',6);

--numero,cf,inizio,fine(può essere null), motivo(può essere null)
UPDATE Cartella_Clinica SET Motivo_ricovero = null WHERE CFpaziente='DGNMRI75B19F688W';
UPDATE Cartella_Clinica SET Motivo_ricovero = null WHERE CFpaziente='SCTTMS56L20F475N';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Infezione toracica' WHERE CFpaziente='PSQVST66P05D539A';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Gastrite' WHERE CFpaziente='LNRMFR82H06L589R';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Insufficienza renale' WHERE CFpaziente='SCIRMN79A30E089Q';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Broncopolmonite' WHERE CFpaziente='RCCMRC83R15H001L';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Tumore al colon' WHERE CFpaziente='FRIRLD76H11H768H';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Meningite' WHERE CFpaziente='GNNSDR66T62D459S';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Infarto' WHERE CFpaziente='CLLTRA62P55D790L';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Tumore alla prostata' WHERE CFpaziente='DFLSSA73P65B506N';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Cisti Dermoidi' WHERE CFpaziente='NGRVNI71E50H042D';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Fibrosarcoma' WHERE CFpaziente='GRFNMR99P44B645Q';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Dermatite atopica' WHERE CFpaziente='MNFVLE84M68C702H';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Emofilia' WHERE CFpaziente='PRRBNG60B25G454K';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Disfagia' WHERE CFpaziente='DRCGST49M20A711T';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Insufficienza cardiaca' WHERE CFpaziente='PRRBRS76P05B902R';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Isospadia' WHERE CFpaziente='MRBRRT82H04C708I';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Leucocitosi' WHERE CFpaziente='BCCGGR56L18L566D';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Medulloblastoma' WHERE CFpaziente='NNNDNT59H02D773R';
UPDATE Cartella_Clinica SET Motivo_ricovero = 'Mononucleosi' WHERE CFpaziente='PPRPFR58R25F182D';


--data,cfchi,cfpaz,tipo,esito,numerosala
INSERT INTO Operazione_Chirurgica Values('2017-11-10','CH-215','MNFVLE84M68C702H','Valvolare aortica','Nessun riscontro negativo',1);
INSERT INTO Operazione_Chirurgica Values('2017-12-29','CH-215','PRRBRS76P05B902R','Embolectomia della polmonare','Necessario rioperare in futuro',2);
INSERT INTO Operazione_Chirurgica Values('2018-02-28','CH-128','PRRBRS76P05B902R','Intervento per craniostenosi','Pieno successo',3);
INSERT INTO Operazione_Chirurgica Values('2018-03-26','CH-748','NNNDNT59H02D773R','Emisferectomia ','Il paziente non riporta nessun dolore, necessari accertamenti',4);
INSERT INTO Operazione_Chirurgica Values('2018-11-02','CH-801','GNNSDR66T62D459S','Fistola congenita dell''esofago','Riuscita',5);
INSERT INTO Operazione_Chirurgica Values('2018-11-05','CH-901','SCTTMS56L20F475N','Artrodesi per via anteriore','Necessari controlli giornalieri',5);


--cf paz, cfoss,consulenzadata,commentomedico
INSERT INTO Consulenza Values('TRRCRS63B25D944H','GDCGVF99A67A273N','2018-12-02','Il paziente presenta disturbi post-trauma, necessarie altre sedute');
INSERT INTO Consulenza Values('PLVLSS87L46C895B','CHSLEI50B08L905Z','2018-12-03','Necessaria Operazione chirurgica dentale');
INSERT INTO Consulenza Values('TRRMRK54P50A268V','PLMGRN79P12E136M','2019-03-26','Ultima seduta, il paziene non ritiene è iù necessaria la terapia, richiesto controllo medico');
INSERT INTO Consulenza Values('NGRVNI71E50H042D','FRVBGT99P60E381E','2019-03-17','Nessun Commento necessario');
INSERT INTO Consulenza Values('PSQVST66P05D539A','GDCGVF99A67A273N','2019-08-06','Richiesto controllo medico urgente');
INSERT INTO Consulenza Values('NNNDNT59H02D773R','PLMGRN79P12E136M','2019-08-29','Consulenza Rimandata');

--matrmed, nome rep
INSERT INTO Assegnazione_Medico Values('MD-159','Cardiologia A');
INSERT INTO Assegnazione_Medico Values('MD-542','Cardiologia A');
INSERT INTO Assegnazione_Medico Values('MD-129','Centro Dialisi');
INSERT INTO Assegnazione_Medico Values('MD-070','Centro Dialisi');
INSERT INTO Assegnazione_Medico Values('MD-712','Endocrinologia');
INSERT INTO Assegnazione_Medico Values('MD-378','Gastroenterologia Digestiva');
INSERT INTO Assegnazione_Medico Values('MD-548','Geriatria');
INSERT INTO Assegnazione_Medico Values('MD-687','Geriatria');
INSERT INTO Assegnazione_Medico Values('MD-375','Medicina Interna');
INSERT INTO Assegnazione_Medico Values('MD-806','Neurochirurgia');
INSERT INTO Assegnazione_Medico Values('MD-159','Cardiologia B');
INSERT INTO Assegnazione_Medico Values('MD-542','Pronto Soccorso - Via Gramsci');
INSERT INTO Assegnazione_Medico Values('MD-578','Medicina Riabilitativa');
INSERT INTO Assegnazione_Medico Values('MD-129','Medicina Riabilitativa');
INSERT INTO Assegnazione_Medico Values('MD-070','Neurochirurgia');
INSERT INTO Assegnazione_Medico Values('MD-712','Medicina Riabilitativa');
INSERT INTO Assegnazione_Medico Values('MD-548','Endocrinologia');
INSERT INTO Assegnazione_Medico Values('MD-578','Endocrinologia');
INSERT INTO Assegnazione_Medico Values('MD-375','Neurochirurgia');

--cfinf, nome rep
INSERT INTO Assegnazione_Infermiere Values('FRRNKT57D14F920I','Cardiologia A');
INSERT INTO Assegnazione_Infermiere Values('FRRNKT57D14F920I','Pronto Soccorso - Via Gramsci');
INSERT INTO Assegnazione_Infermiere Values('FRRNKT57D14F920I','Neurochirurgia');
INSERT INTO Assegnazione_Infermiere Values('MBRFST60M30C240I','Pronto Soccorso - Via Gramsci');
INSERT INTO Assegnazione_Infermiere Values('MBRFST60M30C240I','Geriatria');
INSERT INTO Assegnazione_Infermiere Values('LBRGNR91D18B227A','Pronto Soccorso - Via Gramsci');
INSERT INTO Assegnazione_Infermiere Values('LBRGNR91D18B227A','Chirurgia della Mano');
INSERT INTO Assegnazione_Infermiere Values('LBRGNR91D18B227A','Neurochirurgia');
INSERT INTO Assegnazione_Infermiere Values('LBRGNR91D18B227A','Cardiologia B');
INSERT INTO Assegnazione_Infermiere Values('LBRGNR91D18B227A','Centro Dialisi');
INSERT INTO Assegnazione_Infermiere Values('LNERHL62T42A897A','Chirurgia della Mano');


--codice addetto, nome rep
INSERT INTO Assegnazione_Pulizie Values('789653','Cardiologia A');
INSERT INTO Assegnazione_Pulizie Values('789653','Cardiologia B');
INSERT INTO Assegnazione_Pulizie Values('789653','Medicina Interna');
INSERT INTO Assegnazione_Pulizie Values('481981','Geriatria');
INSERT INTO Assegnazione_Pulizie Values('481981','Gastroenterologia Digestiva');
INSERT INTO Assegnazione_Pulizie Values('481981','Pronto Soccorso - Via Gramsci');


